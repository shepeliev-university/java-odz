package sample;

public class Patient {
    private int id;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String address;
    private int medicalRecordId;
    private String diagnosis;

    public Patient() {
        this.id = 0;
        this.firstName = "";
        this.lastName = "";
        this.patronymic = "";
        this.address = "";
        this.medicalRecordId = 0;
        this.diagnosis = "";
    }

    public Patient(int id, String firstName, String lastName, String patronymic, String address, int medicalRecordId, String diagnosis) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.address = address;
        this.medicalRecordId = medicalRecordId;
        this.diagnosis = diagnosis;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getMedicalRecordId() {
        return medicalRecordId;
    }

    public void setMedicalRecordId(int medicalRecordId) {
        this.medicalRecordId = medicalRecordId;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    @Override
    public String toString() {
        return "" + this.id
                + " Name: "
                + this.lastName + " "
                + this.firstName + " "
                + this.patronymic + " "
                + "\n\tInformation: "
                + "Medical record id - " + this.medicalRecordId
                + ", Diagnosis - " + this.diagnosis
                + ", address - " + this.address;
    }
}
