package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Model {
    public ObservableList<Patient> patientsList = FXCollections.observableArrayList();
    private Connection connection;

    public Model() {
        try {
            this.connection = DatabaseConnection.getInstance().getConnection();
        } catch (SQLException error) {
            System.out.printf(error.toString());
        }
    }

    public ObservableList<Patient> getAll() {
        return this.patientsList;
    }

    public void findAll() {
        try {
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM patiences;");

            patientsList = FXCollections.observableArrayList();

            while (result.next()) {
                Patient patient = new Patient(
                        result.getInt("id"),
                        result.getString("firstName"),
                        result.getString("lastName"),
                        result.getString("patronymic"),
                        result.getString("address"),
                        result.getInt("medicalRecordId"),
                        result.getString("diagnosis")
                );

                patientsList.add(patient);
            }
        } catch (SQLException error) {
            System.out.printf(error.toString());
        }
    }

    public void create(Patient patient) {
        try {
            Statement statement = connection.createStatement();

            String sql = "INSERT INTO patiences "
                    + "VALUES ("
                    + patient.getId() + ", "
                    + "'" + patient.getFirstName() + "'" + ", "
                    + "'" + patient.getLastName() + "'" + ", "
                    + "'" + patient.getPatronymic() + "'" + ", "
                    + "'" + patient.getAddress() + "'" + ", "
                    + patient.getMedicalRecordId() + ", "
                    + "'" + patient.getDiagnosis() + "'"
                    + ");";

            System.out.printf(sql);
            statement.executeUpdate(sql);
            statement.close();

            findAll();
        } catch (SQLException error) {
            System.out.printf(error.toString());
        }
    }

    public void destroy(Patient patient) {
        try {
            String sql = "DELETE FROM patiences WHERE id = " + patient.getId() + ";";

            Statement statement = connection.createStatement();
            statement.executeUpdate(sql);

            statement.executeUpdate(sql);
            findAll();
        } catch (SQLException error) {
            System.out.printf(error.toString());
        }
    }
}
