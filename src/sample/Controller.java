package sample;

import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

public class Controller extends Model {
    private Model patientsList = new Model();

    @FXML
    private Button deleteButton, addButton, editButton, filterButton, resetButton;

    @FXML
    private TableView patientsListView;

    @FXML
    private TableColumn<Patient, Integer> id;

    @FXML
    private TableColumn<Patient, String> firstName;

    @FXML
    private TableColumn<Patient, String> lastName;

    @FXML
    private TableColumn<Patient, String> patronymic;


    @FXML
    private TableColumn<Patient, String> address;
    @FXML
    private TableColumn<Patient, Integer> medicalRecordId;

    @FXML
    private TableColumn<Patient, String> diagnosis;
    @FXML
    private TextField idInput, firstNameInput, lastNameInput, patronymicInput, addressInput, medicalRecordIdInput, diagnosisInput, fromInput, toInput, diagnosisFindInput;

    @FXML
    public void initialize() {
        id.setCellValueFactory(new PropertyValueFactory<Patient, Integer>("id"));
        firstName.setCellValueFactory(new PropertyValueFactory<Patient, String>("firstName"));
        lastName.setCellValueFactory(new PropertyValueFactory<Patient, String>("lastName"));
        patronymic.setCellValueFactory(new PropertyValueFactory<Patient, String>("patronymic"));
        address.setCellValueFactory(new PropertyValueFactory<Patient, String>("address"));
        medicalRecordId.setCellValueFactory(new PropertyValueFactory<Patient, Integer>("medicalRecordId"));
        diagnosis.setCellValueFactory(new PropertyValueFactory<Patient, String>("diagnosis"));

        patientsList.findAll();
        patientsListView.setItems(patientsList.getAll());
    }

    public void handleDeleteClick() {
        int selectedIndex = patientsListView.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            Patient patient = patientsList.getAll().get(selectedIndex);
            patientsList.destroy(patient);
            patientsListView.setItems(patientsList.getAll());
        }
    }

    public void handleEditClick() {
        Patient person = (Patient) patientsListView.getSelectionModel().getSelectedItem();
        int selectedIndex = patientsListView.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            Patient patient = patientsList.getAll().get(selectedIndex);
            patientsList.destroy(patient);
            patientsListView.setItems(patientsList.getAll());
        }

        idInput.setText(Integer.toString(person.getId()));
        lastNameInput.setText(person.getLastName());
        firstNameInput.setText(person.getFirstName());
        patronymicInput.setText(person.getPatronymic());
        addressInput.setText(person.getAddress());
        diagnosisInput.setText(person.getDiagnosis());
        medicalRecordIdInput.setText(Integer.toString(person.getMedicalRecordId()));
    }

    @SuppressWarnings("unchecked")
    public void handleAddClick(ActionEvent event) {
        Patient patient = new Patient();

        if (idInput.getText().length() != 0 && idInput.getText().matches("[0-9]*")) {
            patient.setId(Integer.parseInt(idInput.getText()));
        }

        if (firstNameInput.getText().length() != 0) {
            patient.setFirstName(firstNameInput.getText());
        }

        if (lastNameInput.getText().length() != 0) {
            patient.setLastName(lastNameInput.getText());
        }

        if (patronymicInput.getText().length() != 0) {
            patient.setPatronymic(patronymicInput.getText());
        }

        if (addressInput.getText().length() != 0) {
            patient.setAddress(addressInput.getText());
        }

        if (medicalRecordIdInput.getText().length() != 0) {
            patient.setMedicalRecordId(Integer.parseInt(medicalRecordIdInput.getText()));
        }

        if (diagnosisInput.getText().length() != 0) {
            patient.setDiagnosis(diagnosisInput.getText());
        }

        patientsList.create(patient);
        patientsListView.setItems(patientsList.getAll());

        idInput.clear();
        lastNameInput.clear();
        firstNameInput.clear();
        patronymicInput.clear();
        addressInput.clear();
        diagnosisInput.clear();
        medicalRecordIdInput.clear();
    }

    public void handleResetClick() {
        patientsList.findAll();
        patientsListView.setItems(patientsList.getAll());
    }

    public void handleFilterClick() {
        Integer fromId = Integer.parseInt(fromInput.getText());
        Integer toId = Integer.parseInt(toInput.getText());

        FilteredList<Patient> filteredData = new FilteredList<Patient>(patientsList.getAll(), patient -> true);

        filteredData.setPredicate((patient) -> {
            Integer medicalRecordId = patient.getMedicalRecordId();
            return fromId <= medicalRecordId && medicalRecordId <= toId;
        });

        patientsListView.setItems(filteredData);
    }

    public void handleFindClick() {
        String lookedDiagnosis = diagnosisFindInput.getText();
        patientsList.findAll();

        FilteredList<Patient> filteredData = new FilteredList<Patient>(patientsList.getAll(), patient -> true);

        filteredData.setPredicate((patient) -> {
            String diagnosis = patient.getDiagnosis();
            return lookedDiagnosis.toLowerCase().equals(diagnosis.toLowerCase());
        });

        patientsListView.setItems(filteredData);
    }
}





